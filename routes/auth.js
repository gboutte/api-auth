const router = require('express').Router();
const User = require('../model/User');
const {registerValidation,loginValidation} = require('../validation/auth');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');

function getJwtForUser(user){
	const token = jwt.sign(
		{
			_id:user._id,
		},
		process.env.TOKEN_SECRET
	);

	return token;

}

/**
 * @api {post} /user/register Create a new user
 * @apiName Register
 * @apiGroup User
 *
 * @apiParam {String} email Email address of the User.
 * @apiParam {String} username Username of the User.
 * @apiParam {String} password The password of the User.
 *
 * @apiVersion 1.0.0
 * @apiSuccess {String} token Authorization token to use for other requests .
 */
router.post('/register',async (req,res)=>{

	//validation
	const {error} = registerValidation(req.body);
	if(error) return res.status(400).json({error:error.details[0].message});


	//Checking if user is alreadu in database
	const emailExist = await User.findOne({
		email:req.body.email
	});

	if(emailExist) return res.status(400).json({error:'Email already exists'})

	//Hash the password
	const salt = await bcrypt.genSalt(10);
	const hashedPassword = await bcrypt.hash(req.body.password,salt);

	//Create a new user
	const user = new User({
		username:req.body.username,
		email:req.body.email,
		password:hashedPassword
	});

	try{
		const savedUser = await user.save();
		const token = getJwtForUser(savedUser);
		res.json({token:token});
	}catch(err){
		res.status(400).json({error:err});
	}

});

/**
 * @api {post} /user/login Get authorization token for a user
 * @apiName Login
 * @apiGroup User
 *
 * @apiParam {String} email Email address of the User.
 * @apiParam {String} password The password of the User.
 *
 * @apiVersion 1.0.0
 * @apiSuccess {String} token Authorization token to use for other requests .
 */
router.post('/login', async (req,res)=>{
	
	const wrongCreditentialMsg = "Email or password is wrong";
	//validation
	const {error} = loginValidation(req.body);
	if(error) return res.status(400).json({error:error.details[0].message});

	//Checking if user exist
	const user = await User.findOne({
		email:req.body.email
	});

	if(!user) return res.status(400).json({error:wrongCreditentialMsg})


	const validPassword = await bcrypt.compare(req.body.password,user.password);
	
	if(!validPassword) return res.status(400).json({error:wrongCreditentialMsg})

	//Create and assign a token
	const token = getJwtForUser(user);
	res.header('auth-token',token).json({token:token});

});

module.exports = router;