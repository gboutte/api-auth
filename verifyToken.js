const jwt = require('jsonwebtoken');
const debug = require('./debug');
const User = require('./model/User');

module.exports = async function auth(req,res,next){
	debug('Starting','jwt');
	const token = req.header('auth-token');
	if(!token){
		debug('No token','jwt');
		return res.status(401).json({error:'Access Denied'});
	}
	try{
		const verified = jwt.verify(token,process.env.TOKEN_SECRET);
		const user = await User.findOne({_id:verified._id});
		req.user = user;
		debug('User '+user.username+" is verified","jwt");
		next();
	}catch(err){
		res.status(401).json({error:'Access Denied'});
	}

};