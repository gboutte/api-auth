# Auth api node js express [![pipeline status](https://gitlab.com/gboutte/api-auth/badges/master/pipeline.svg)](https://gitlab.com/gboutte/api-auth/commits/master) [![coverage report](https://gitlab.com/gboutte/api-auth/badges/master/coverage.svg)](https://gitlab.com/gboutte/api-auth/commits/master)

A basic nodejs api with JWT auth

## Installation

```sh
npm install
```

## Configuration
Here is a .env exemple

```
DB_CONNECT=mongodb://localhost:27017/dbname
TOKEN_SECRET=secret
DEBUG=true
```
## Api doc

```sh
apidoc -i api-auth/ -o apidoc/ -e api-auth/node_modules/
```

## Tests

```sh
npm test
```