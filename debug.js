

module.exports = (data,prefix = null)=>{
	if(process.env.DEBUG == "true" && process.env.TESTING != "true"){
		if(prefix){

		console.log("["+prefix.toUpperCase()+"] "+data);
		}else{
			console.log(data);
		}
	}
}