const router = require('express').Router();
const verifyToken = require('../verifyToken');

/**
 * @api {get} /private An example of private route
 * @apiName Private route
 * @apiGroup Example
 *
 * @apiHeader {String} auth-token The authorization token.
 *
 * @apiVersion 1.0.0
 * @apiSuccess {String} message A example of message
 */
router.get('/',verifyToken,(req,res)=>{
	res.json({message:"You're logged as "+req.user.username});
});


module.exports = router;