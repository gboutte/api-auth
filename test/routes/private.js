
const chai = require('chai');
const chaiHttp = require('chai-http');
const expect = chai.expect;
const conn = require('../../db'); // <-- db.js
const app = require('../../app'); // <-- app.js

chai.use(chaiHttp);

describe('Private route', () => {


    before(function(done) {
        conn.open().then(() => done()).catch(done);
    });

    after(function(done){
        conn.close().then(() => done()).catch(done);
    });

    it('Should be denied without auth', (done) => {
        chai.request(app)
            .get('/private')
            .end((error, response) => {
                if (error) done(error);
                // Now let's check our response
                expect(response).to.have.status(401);
                expect(response.body).to.have.property('error');
                done();
            });
    });


    it('Should be denied without a valid token', (done) => {
        chai.request(app)
            .get('/private')
            .set('auth-token', "wrong token")
            .end((error, response) => {
                if (error) done(error);
                // Now let's check our response
                expect(response).to.have.status(401);
                expect(response.body).to.have.property('error');
                done();
            });
    });

    it("Should be allowed to access private route with the good token",(done)=>{
            chai.request(app)
            .post('/user/register')
            .send({
                username:"private",
                email:"private@company.com",
                password:"123456"
            })
            .end(function (err, res) {
                expect(err).to.be.null;
                var token = res.body.token;

                chai.request(app)
                .get('/private')
                .set('auth-token', token)
                .end((error, response) => {
                    if (error) done(error);
                    // Now let's check our response
                    expect(response).to.have.status(200);
                    expect(response.body).to.have.property('message');
                    done();
                });
            });

    })
});