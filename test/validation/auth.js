const {registerValidation,loginValidation} = require('../../validation/auth');
const expect = require('chai').expect;


describe("Validation auth register",()=>{

	describe("Should be valid",()=>{
		it("Should accept a valid user",()=>{

			const {error} = registerValidation({
				email: "test@company.com",
				username:"MisterTest",
				password:"123456"

			});
			
			expect(error).to.be.undefined;
		});

	})

	describe("Sould be invalid",()=>{
		it("Shouldn't accept email without @",()=>{
			const {error} = registerValidation({
				email: "testcompany.com",
				username:"MisterTest",
				password:"123456"

			});

			expect(error).to.have.property('details');
		})
		
		it("Shouldn't accept email without a dot ext",()=>{
			const {error} = registerValidation({
				email: "test@company",
				username:"MisterTest",
				password:"123456"

			});
			
			expect(error).to.have.property('details');
		})
		

		it("Shouldn't accept without an email",()=>{
			const {error} = registerValidation({
				username:"MisterTest",
				password:"123456"

			});
			

			expect(error).to.have.property('details');
		})

		it("Shouldn't accept without a username",()=>{
			const {error} = registerValidation({
				email:"test@company.fr",
				password:"123456"

			});
			

			expect(error).to.have.property('details');
		})

		it("Shouldn't accept without a password",()=>{
			const {error} = registerValidation({
				email:"test@company.fr",
				username:"MisterTest"

			});
			

			expect(error).to.have.property('details');
		})
		
	})
	

	
	

});

describe("Validation auth login",()=>{

	
	describe("Sould be valid",()=>{
		it("Should accept a valid login",()=>{

			const {error} = loginValidation({
				email: "test@company.com",
				password:"123456"

			});
			
			expect(error).to.be.undefined;

		});
	})

	describe("Should be invalid",()=>{
		it("email without @",()=>{
			const {error} = loginValidation({
				email: "testcompany.com",
				password:"123456"

			});
			
			expect(error).to.have.property('details');
		})
		it("Shouldn't accept email without a dot ext",()=>{
			const {error} = loginValidation({
				email: "test@company",
				password:"123456"

			});
			
			expect(error).to.have.property('details');
		})

		it("Shouldn't accept without a password",()=>{
			const {error} = loginValidation({
				email:"test@company.fr"

			});
			

			expect(error).to.have.property('details');
		})
		it("Shouldn't accept without an email",()=>{
			const {error} = loginValidation({
				password:"123456"

			});
			

			expect(error).to.have.property('details');
		})

	})
	
	
	
});