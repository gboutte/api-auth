const express = require('express');
const app = express();
const dotenv = require('dotenv');
const debug = require('./debug');
const router = require('./routes');

dotenv.config();

//Middleware
app.use(express.json());

//Route middleware
app.use('/',router);


module.exports = app;