const chai = require('chai');
const expect = chai.expect;
const chaiHttp = require('chai-http');

chai.use(chaiHttp);

const app = require('../../app.js');

describe('Public route', () => {
    it('Should be allow without auth', (done) => {
        chai.request(app)
            .get('/public')
            .end((error, response) => {
                if (error) done(error);
                // Now let's check our response
                expect(response).to.have.status(200);
                expect(response.body).to.have.property('message');
                done();
            });
    });
});