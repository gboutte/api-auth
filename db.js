const mongoose = require('mongoose');
const {MongoMemoryServer} = require('mongodb-memory-server')
mongoose.Promise = global.Promise;
var mongod = null;


function open(){
    return new Promise( async(resolve, reject) => {
        if(process.env.TESTING == "true") {
            mongod = new MongoMemoryServer()

            const uri = await mongod.getConnectionString();
            mongoose.connect(
              uri,
              { useUnifiedTopology: true,useNewUrlParser:true } ,
              (err,res)=>{
                

                if (err) return reject(err);
                resolve();
              }
            );
        }else{
            

            mongoose.connect(
              process.env.DB_CONNECT,
              { useUnifiedTopology: true,useNewUrlParser:true } ,
              (err,res)=>{
                
                if (err) return reject(err);
                resolve();
              }
            );


        }
    });
}

function close(){
    if(process.env.TESTING == "true") {
      mongod.stop();
    }
    return mongoose.disconnect();
}

module.exports = { close, open };