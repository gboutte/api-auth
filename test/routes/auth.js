
const chai = require('chai');
const chaiHttp = require('chai-http');
const expect = chai.expect;
const conn = require('../../db'); // <-- db.js
const app = require('../../app'); // <-- app.js

chai.use(chaiHttp);

describe('Auth', function(){

    before(function(done) {
        conn.open().then(() => done()).catch(done);
    });

    after(function(done){
        conn.close().then(() => done()).catch(done);
    });


	describe("Register",()=>{
		it("When we register we get a token", function(done){

	     	chai.request(app)
			.post('/user/register')
			.send({
		    	username:"gboutte",
		    	email:"gboutte@company.com",
		    	password:"123456"
		    })
			.end(function (err, res) {
				expect(err).to.be.null;
			    expect(res).to.have.status(200);

			    expect(res.body).to.have.property("token");
			    
	        	done();
			});
	    });

	    it("When we register with a taken email we get an error", function(done){

	     	chai.request(app)
			.post('/user/register')
			.send({
		    	username:"jean",
		    	email:"gboutte@company.com",
		    	password:"8454545"
		    })
			.end(function (err, res) {
				expect(err).to.be.null;
			    expect(res).to.have.status(400);

			    expect(res.body).to.have.property("error");
			    
	        	done();
			});
	    });
	})
    
    describe("Login",()=>{
    	it("When we login with right cred we get a token", function(done){

	     	chai.request(app)
			.post('/user/login')
			.send({
		    	email:"gboutte@company.com",
		    	password:"123456"
		    })
			.end(function (err, res) {
				expect(err).to.be.null;
			    expect(res).to.have.status(200);

			    expect(res.body).to.have.property("token");
	        	done();
			});
	    });
	    it("When we login with wrong cred we get an error", function(done){

	     	chai.request(app)
			.post('/user/login')
			.send({
		    	email:"gboutte@company.com",
		    	password:"8454545"
		    })
			.end(function (err, res) {
				expect(err).to.be.null;
			    expect(res).to.have.status(400);

			    expect(res.body).to.have.property("error");
			    
	        	done();
			});
	    });


    })

});