const router = require('express').Router();

/**
 * @api {get} /public An example of public route
 * @apiName Public route
 * @apiGroup Example
 *
 * @apiVersion 1.0.0
 * @apiSuccess {String} message A example of message
 */
router.get('/',(req,res)=>{
	res.json({message:"I don't know if you're logged, but it's ok"});
});


module.exports = router;