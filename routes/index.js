const router = require('express').Router();
//import routes
const authRoute = require('./auth');
const privateRoute = require('./private');
const publicRoute = require('./public');


router.use('/user',authRoute);
router.use('/private',privateRoute);
router.use('/public',publicRoute);

//Default route
router.use((req,res)=>{
	res.status(404).json({error:"Route not found"});
});

module.exports = router;