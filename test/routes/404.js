const chai = require('chai');
const expect = chai.expect;
const chaiHttp = require('chai-http');

chai.use(chaiHttp);

const app = require('../../app.js');

describe('404 route', () => {
    it('Unknown route', (done) => {
        chai.request(app)
            .get('/sdhifgsdfkdsfbdsfbdskfqdfqsdbkvqdsv')
            .end((error, response) => {
                if (error) done(error);
                // Now let's check our response
                expect(response).to.have.status(404);
                expect(response.body).to.have.property('error');
                done();
            });
    });
});