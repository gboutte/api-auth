const debug = require('./debug');
const app = require('./app');
const mongoose = require('mongoose');
const conn = require('./db')
conn.open().then(() => {

	debug('Connnect to mongodb','info')

	app.listen(process.env.PORT|3000, ()=>{
		debug('Server is up','info');
	});

}).catch((err)=>{
	debug(err)
});

